# Boniface: A Personal Lexicon Manager

Boniface is a simple lightweight desktop application written in Java for managing a collection of words, it is a tool to facilitate the writing of prose and poetry. I developed it for myself after finding other solutions unsatisfying. The GUI is in JavaFX, and words are saved in a single SQLite database file.

## Current Features
- add/edit/delete words and descriptions
- create associations and tags
- view/search word list
- create/open database
- export to HTML, CSV, TXT
- lookup word in wiktionary.org / etymonline.com / etc

## Future Features
- manage list of lookups, allow custom lookups
- dark / light / custom CSS
- context menu on word list
- use markdown for description (it now uses plain text)
- visualize word web/clusters

